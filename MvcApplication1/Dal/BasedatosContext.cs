﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MvcApplication1.Models;

namespace MvcApplication1.Dal
{
    public class BasedatosContext : DbContext
    {
        public BasedatosContext() :
            base("BasedatosContext")
        {

        }
        public DbSet<CorreoValidacion> Mensajes { get; set; }

    }   
    
}
