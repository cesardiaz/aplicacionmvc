﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class CorreoValidacion
    {
        public int ID { get; set; }
        [Required]
        public string nombre { get; set; }
        public string apellido { get; set; }
        [Required]
        [EmailAddress]
        public string email { get; set; }
        [Required]
        public string mensaje { get; set; }
    


    }
}