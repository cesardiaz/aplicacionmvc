﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Correo
    {
        public int ID { get; set; }
        public string nombre{get;set;}
        public string apellidos{get;set;}
        public string email { get; set; }
        public string mensaje { get; set; }

    }
}