﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;
using MvcApplication1.Dal;

namespace MvcApplication1.Controllers
{
    public class Emailv3Controller : Controller
    {
        //
        // GET: /Emailv3/
        BasedatosContext contexto = new BasedatosContext();

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Sendmail(CorreoValidacion CorreoValidacion)
        {
            if (!ModelState.IsValid)

                return View("Index");
            try
            {
                contexto.Mensajes.Add(CorreoValidacion);
                contexto.SaveChanges();
                return View("Enviado", CorreoValidacion);
            }
            catch
            {
                return View();
            }

        }
        public ActionResult Details(int id)
        {
            try
            {
                var mensaje = contexto.Mensajes.Where(m => m.ID == id).FirstOrDefault();
                if (mensaje == null)
                {
                    return View();
                }
                else
                {
                    return View("Detalles", mensaje);
                }
            }
            catch
            {
                return View();
            }



        }
       
        public ActionResult Edit(int id)
        {
            try
            {
                var mensaje = contexto.Mensajes.Where(m => m.ID == id).FirstOrDefault();
                if (mensaje == null)
                {
                    return View();
                }
                else
                {
                    return View("Editar", mensaje);
                    
                    
                }

            }
            catch
            {
                return View();
            }
        }
       
        public ActionResult GuardarCambios(CorreoValidacion Correo)
        {
            if (!ModelState.IsValid)
                return View("Index");
                
            try
            {
                
                
                
                    contexto.Entry(Correo).State = EntityState.Modified;
                    return View("List", contexto.Mensajes);
                
            }
            catch
            {
                return View();
            }




        }




        public ActionResult List()
        {
            return View(contexto.Mensajes);
        }
        public ActionResult Delete(int id)
        {
           
            try
            {
                var mensaje = contexto.Mensajes.Where(m => m.ID == id).FirstOrDefault();
                               
                return View("Borrado", mensaje);
                

                 

              
                


            }
            catch
            {
                return View();
            }
        }
        
        public ActionResult Borrar(int id)
        {
           
            try
            {
                var correo = contexto.Mensajes.Find(id);
                contexto.Mensajes.Remove(correo);
                contexto.SaveChanges();
                return View("List", contexto.Mensajes);
            }
            catch
            {
                return View();
            }

        }
        
    
        
        public ActionResult CambiarLenguaje(string Lang)
        {
            var cultura = new System.Globalization.CultureInfo(Lang);
            System.Threading.Thread.CurrentThread.CurrentCulture = cultura;
            System.Threading.Thread.CurrentThread.CurrentUICulture = cultura;
            return View("Index");
        }
     

    }

}
