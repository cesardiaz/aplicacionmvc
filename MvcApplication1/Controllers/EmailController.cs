﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class EmailController : Controller
    {
        //
        // GET: /Email/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Send(FormCollection collection)
        {
            try
            {
                var nombre = collection["nombre"];
                var apellidos = collection["apellidos"];
                var email = collection["email"];
                var mensaje = collection["mensaje"];
                return RedirectToAction("Enviado",
                    new
                    {
                        nombre = nombre,
                        apellidos = apellidos,
                        email = email,
                        mensaje = mensaje

                    });

            }
            catch
            {
                return View();
            }
        }

        public ActionResult Enviado(string nombre, string apellidos, string email, string mensaje)
        {
            ViewBag.nombre = nombre;
            ViewBag.apellidos = apellidos;
            ViewBag.email = email;
            ViewBag.mensaje = mensaje;
            return View();
        }
    }
}


    




       
